package com.training.northwind.service;

import com.training.northwind.model.Shipper;
import com.training.northwind.repository.ShipperRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ShipperService {

    @Autowired
    ShipperRepo repo;

    public List<Shipper> findAll() { return repo.findAll(); }

    public Optional<Shipper> findById(long id) {
        return repo.findById(id);
    }

    public List<Shipper> findByPhone(String phone) {
        return repo.findByPhone(phone);
    }

    public Shipper save(Shipper shipper) {
        return repo.save(shipper);
    }
}
