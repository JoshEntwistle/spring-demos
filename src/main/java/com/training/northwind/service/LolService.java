package com.training.northwind.service;

import com.training.northwind.model.LolChampion;
import com.training.northwind.repository.LolRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LolService {
    @Autowired
    LolRepo repo;

    public List<LolChampion> findAll() { return repo.findAll(); }
}
