package com.training.northwind.repository;

import com.training.northwind.model.Shipper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShipperRepo extends JpaRepository<Shipper, Long> {
    List<Shipper> findByPhone(String phone); // for demo only
}
