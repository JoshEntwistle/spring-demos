package com.training.northwind.repository;

import com.training.northwind.model.LolChampion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LolRepo extends JpaRepository<LolChampion, Long> { }
