package com.training.northwind.controller;

import com.training.northwind.model.Shipper;
import com.training.northwind.service.ShipperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/shipper")
public class ShipperController {

    @Autowired
    private ShipperService service;

    @GetMapping
    public List<Shipper> findAll() { return service.findAll(); }

    @GetMapping("/{id}")
    public ResponseEntity<Shipper> findById(@PathVariable long id) {
        return service.findById(id)
                .map(shipper -> new ResponseEntity<>(shipper, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    public ResponseEntity<Shipper> create(Shipper shipper) {
        return new ResponseEntity<>(service.save(shipper), HttpStatus.CREATED);
    }
}
