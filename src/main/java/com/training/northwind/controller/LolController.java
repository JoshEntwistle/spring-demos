package com.training.northwind.controller;

import com.training.northwind.model.LolChampion;
import com.training.northwind.service.LolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/lol")
public class LolController {

    @Autowired
    private LolService service;

    @GetMapping()
    public List<LolChampion> findAll() { return service.findAll(); }
}
