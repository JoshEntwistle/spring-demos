package com.training.northwind.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ShipperTest {

    private final Shipper shipper = new Shipper();

    @Test
    void setGetPhone() {
        String phone = "123";
        shipper.setPhone(phone);
        assertEquals(phone, shipper.getPhone());
    }

}